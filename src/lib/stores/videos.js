import { derived, writable } from 'svelte/store';

export const videos = writable(null);

export async function refresh_videos() {
  const response = await fetch('videos.json', {headers: {'Accept': 'application/json'}});
  if (response.ok) {
    const data = await response.json();
    videos.set(data);
    return data;
  } else {
    throw new Error("Unable to retrieve the videos");
  }
}

export const idx_videos = derived(
  videos,
  ($videos) => {
    const idx_videos = {};

    for (const v of $videos) {
      idx_videos[v.id] = v;
    }

    return idx_videos;
  }
);
