import { refresh_videos } from '$lib/stores/videos.js';

export const ssr = false;

export async function load() {
  let error = null;

  refresh_videos().then(() => true, (err) => error = err.detail);

  return {
    error
  };
}
